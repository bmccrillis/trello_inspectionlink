//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Version
    {
        public int VersionKey { get; set; }
        public Nullable<int> Version1 { get; set; }
        public string Version_Name { get; set; }
        public string Version_Number { get; set; }
        public Nullable<System.DateTime> Version_Date { get; set; }
        public bool Conversion_DB { get; set; }
        public string Version_PVCS { get; set; }
        public System.DateTime Last_Updated { get; set; }
        public string INI_Rev { get; set; }
    }
}
