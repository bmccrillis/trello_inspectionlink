//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Change_History
    {
        public string Change_History1 { get; set; }
        public string Change_Owner { get; set; }
        public Nullable<short> Requested_By { get; set; }
        public Nullable<short> Change_Type { get; set; }
        public Nullable<bool> External_Notif { get; set; }
        public Nullable<bool> Internal_Notif { get; set; }
        public Nullable<bool> ShipDate_Changed { get; set; }
        public Nullable<bool> ProdDate_Changed { get; set; }
        public string Reason { get; set; }
        public string Notes { get; set; }
        public string Changed_By { get; set; }
        public Nullable<System.DateTime> Change_Date { get; set; }
        public Nullable<System.DateTime> Old_Date { get; set; }
        public Nullable<System.DateTime> New_Date { get; set; }
        public Nullable<double> Old_Number { get; set; }
        public Nullable<double> New_Number { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
        public string Old_Text { get; set; }
        public string New_Text { get; set; }
        public string WC_Vendor { get; set; }
        public string Operation_Service { get; set; }
        public string Job { get; set; }
    }
}
