//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bill_Of_Quotes
    {
        public string Parent_Quote { get; set; }
        public string Component_Quote { get; set; }
        public Nullable<int> Quote_Operation { get; set; }
        public string Relationship_Type { get; set; }
        public Nullable<int> Relationship_Qty { get; set; }
        public System.DateTime Last_Updated { get; set; }
    
        public virtual Quote Quote { get; set; }
        public virtual Quote Quote1 { get; set; }
    }
}
