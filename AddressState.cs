//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class AddressState
    {
        public System.Guid ObjectID { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
    }
}
