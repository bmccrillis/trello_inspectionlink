//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkCenterStat
    {
        public WorkCenterStat()
        {
            this.Notes = new HashSet<Note>();
        }
    
        public int Job_Operation_Key { get; set; }
        public string Status { get; set; }
        public string Group { get; set; }
        public string WCOwner { get; set; }
        public Nullable<int> WCStat { get; set; }
        public string WC_Vendor { get; set; }
        public string CurrentOp_Status { get; set; }
        public string NextOp { get; set; }
        public string NextOP_Status { get; set; }
        public string Job_Number { get; set; }
        public string Customer { get; set; }
        public Nullable<System.DateTime> Date_Created { get; set; }
        public Nullable<System.DateTime> Date_Modified { get; set; }
        public Nullable<int> Queue_Days { get; set; }
        public Nullable<int> On_Report_Days { get; set; }
        public string Fault_Code { get; set; }
        public string LatestNote { get; set; }
        public Nullable<System.DateTime> LatestNoteDate { get; set; }
        public Nullable<bool> Alert { get; set; }
        public string Alert_Code { get; set; }
        public Nullable<System.DateTime> Alert_Created { get; set; }
        public Nullable<System.DateTime> PromiseDate { get; set; }
        public Nullable<bool> PrintBatch { get; set; }
        public string Location { get; set; }
        public Nullable<System.DateTime> LocationDate { get; set; }
        public Nullable<System.DateTime> RequestedDate { get; set; }
    
        public virtual Fault_Codes Fault_Codes { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }
}
