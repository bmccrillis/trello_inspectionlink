//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Attendance
    {
        public string Attendance1 { get; set; }
        public string Employee { get; set; }
        public Nullable<System.DateTime> Work_Date { get; set; }
        public Nullable<System.DateTime> Login { get; set; }
        public Nullable<System.DateTime> Adjusted_Login { get; set; }
        public Nullable<System.DateTime> Logout { get; set; }
        public Nullable<System.DateTime> Adjusted_Logout { get; set; }
        public Nullable<double> Regular_Minutes { get; set; }
        public Nullable<double> OT_Minutes { get; set; }
        public Nullable<double> DoubleOT_Minutes { get; set; }
        public Nullable<double> Break_Minutes { get; set; }
        public Nullable<int> Attendance_Type { get; set; }
        public Nullable<bool> Lock_Times { get; set; }
        public Nullable<int> Source { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
    }
}
