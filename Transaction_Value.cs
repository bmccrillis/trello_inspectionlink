//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction_Value
    {
        public string Transaction_Value1 { get; set; }
        public Nullable<int> Data_Type { get; set; }
        public string Default_Value { get; set; }
        public string Display_Text { get; set; }
        public Nullable<bool> Hidden { get; set; }
        public Nullable<int> Max_Value { get; set; }
        public Nullable<int> Min_Value { get; set; }
        public Nullable<int> Sequence { get; set; }
        public Nullable<int> String_Length { get; set; }
        public Nullable<bool> System_Required { get; set; }
        public Nullable<bool> User_Required { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
        public Nullable<int> Transaction_Definition { get; set; }
        public Nullable<bool> DB_Validation { get; set; }
        public Nullable<bool> Confirmation_Prompt { get; set; }
        public string Transaction_Field { get; set; }
    }
}
