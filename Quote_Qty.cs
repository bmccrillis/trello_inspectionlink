//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Quote_Qty
    {
        public int Quote_QtyKey { get; set; }
        public Nullable<int> Quote_Qty_Key { get; set; }
        public string Quote { get; set; }
        public Nullable<int> Parent_Quote_Qty_Key { get; set; }
        public Nullable<int> Quote_Qty1 { get; set; }
        public Nullable<double> Yield_Pct { get; set; }
        public Nullable<int> Make_Quantity { get; set; }
        public Nullable<double> Est_Setup_Hrs { get; set; }
        public Nullable<double> Est_Run_Hrs { get; set; }
        public Nullable<double> Est_Total_Hrs { get; set; }
        public Nullable<decimal> Est_Labor { get; set; }
        public Nullable<decimal> Est_Material { get; set; }
        public Nullable<decimal> Est_Service { get; set; }
        public Nullable<decimal> Est_Labor_Burden { get; set; }
        public Nullable<decimal> Est_Machine_Burden { get; set; }
        public Nullable<decimal> Est_GA_Burden { get; set; }
        public Nullable<double> Unit_Price { get; set; }
        public Nullable<double> Quoted_Unit_Price { get; set; }
        public Nullable<decimal> Total_Price { get; set; }
        public string Price_UofM { get; set; }
        public Nullable<double> Profit_Pct { get; set; }
        public Nullable<double> Labor_Markup_Pct { get; set; }
        public Nullable<double> Mat_Markup_Pct { get; set; }
        public Nullable<double> Serv_Markup_Pct { get; set; }
        public double Labor_Burden_Markup_Pct { get; set; }
        public double Machine_Burden_Markup_Pct { get; set; }
        public double GA_Burden_Markup_Pct { get; set; }
        public Nullable<short> Open_Operations { get; set; }
        public string Note_Text { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
        public string Order_Unit { get; set; }
        public Nullable<double> Price_Unit_Conv { get; set; }
        public Nullable<int> Decimal_Places { get; set; }
        public Nullable<int> Price_Source { get; set; }
        public Nullable<bool> Locked_Source { get; set; }
    
        public virtual Quote Quote1 { get; set; }
    }
}
