//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sched_Bill_Dist
    {
        public string Sched_Bill_Dist1 { get; set; }
        public string Scheduled_Bill { get; set; }
        public double Amount { get; set; }
        public string Description { get; set; }
        public string GL_Account { get; set; }
        public System.DateTime Last_Updated { get; set; }
        public Nullable<bool> Discountable { get; set; }
    }
}
