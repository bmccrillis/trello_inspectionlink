//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class WorkCenter
    {
        public int JobOperation { get; set; }
        public string JobNumber { get; set; }
        public string WCVendor { get; set; }
        public Nullable<double> EstSetup { get; set; }
        public Nullable<double> EstRun { get; set; }
        public Nullable<bool> Inside { get; set; }
        public string Status { get; set; }
        public System.DateTime DateAdded { get; set; }
        public Nullable<System.DateTime> DateCompleted { get; set; }
        public int DayCount { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
    }
}
