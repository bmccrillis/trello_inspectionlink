﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trello_InspectionLink
{
    class Class_CardJobNumbers
    {
        public string cardName { get; set; }
        public string cardIDQuid { get; set; }
        public string cardJobNum { get; set; }
        public DateTime cardJobOPStartDate { get; set; }
        public int JobOperationKey { get; set; }
        public Boolean AddNoteToCard { get; set; }
        public string NoteForCard { get; set; }
        public string QueueTrackerDays { get; set; }
        public string ProductionReportQueueDays { get; set; }
        public string CardNameWithOutTags { get; set; }

        public Class_CardJobNumbers()
        {

        }
    }
}
