﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Manatee.Trello;
using Manatee.Trello.ManateeJson;
using Manatee.Trello.RestSharp;
using Manatee.Trello.Rest;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Diagnostics;
//using System.Threading;

namespace Trello_InspectionLink
{
    class Program
    {
        static void Main(string[] args)
        {
            //try
            //{
                DateTime timestamp = DateTime.Now;
                //use this to search only one card

                //String CardNumber = "A954";

                var serializer = new ManateeSerializer();
                TrelloConfiguration.Serializer = serializer;
                TrelloConfiguration.Deserializer = serializer;
                TrelloConfiguration.JsonFactory = new ManateeFactory();
                TrelloConfiguration.RestClientProvider = new RestSharpClientProvider();
                TrelloAuthorization.Default.AppKey = "80f78902b85e7befdaf58afbcfcaf8ad";
                TrelloAuthorization.Default.UserToken = "81e5eb9dd1c81e24bd6fe5fbbaf90e50f3cd3db50f777427158044a7facaeeb1";

            //chris' key
                //TrelloAuthorization.Default.AppKey = "f104a26529d5a337fc129e8acb911680";
                //TrelloAuthorization.Default.UserToken = "20b26d6126148920bfbe99ff865311e455450644cb8d1e62e81bfb695cad2cd5";



                //get solution token: https://trello.com/1/authorize?key=80f78902b85e7befdaf58afbcfcaf8ad&name=Trello_InspectionLink&expiration=never&response_type=token&scope=read,write
                // Get Organizations @ https://trello.com/1/members/me/organizations?fields=name
            //{"id":"5223900ad11090d24e006ffb","name":"acutec"},
            //{"id":"55157336e763476a1370049c","name":"acutecci"},
            //{"id":"5565a3e86da3c1bda0e63243","name":"acutecdeburring"},
            //{"id":"5576f65bf7f77bc6812680f8","name":"acutecenf"},
            //{"id":"5523c71a6c0e27028a579100","name":"acutecengineering"},
            //{"id":"5448f70e648afb88d7c6fdc6","name":"acutecprecisionmachiningit"},
            //{"id":"5575cfae3e65f3ebc4657422","name":"acutecpurchasing"},
            //{"id":"555250f1bf83a70dc1196661","name":"acutecqa"},
            //{"id":"5578757f8a7b231575894619","name":"acutecquality"},
            //{"id":"5571fbb3e31e72208e828387","name":"acutecsqe"},
            //{"id":"557700c81f6de43fb64352bf","name":"acutecshared"},
            //{"id":"5554c3b8c0aa0ee4aef09fe7","name":"acutecshipping"},
            //{"id":"55781c55914d4912d51b07b6","name":"acutecwelcome"}

                //55533cdfad9518b599f2e146

                //TrelloConfiguration.ExpiryTime = new TimeSpan(0, 5, 0);
                var orgID = "555250f1bf83a70dc1196661";
                //var orgID = "55157336e763476a1370049c";
                //var orgID = "5565a3e86da3c1bda0e63243";
                var org = new Organization(orgID);
                //var TemplateMembers = org.Members.ToList();
                //var InspectionBoard = org.Boards.Where(x => x.Id == "55533cdfad9518b599f2e146");            

                //var orgID = "5523c71a6c0e27028a579100";
                var TemplateMembers = org.Members.ToList();
                Console.WriteLine(org.Name);
                //var stopwatch = Stopwatch.StartNew();
                var Inspectionboards = org.Boards;
                int i = Inspectionboards.Count();
                foreach (var Inspectionboard in Inspectionboards)
                {
                  if (Inspectionboard.Name.ToLower().Contains("inspect") || Inspectionboard.Name.ToLower().Contains("qe") || Inspectionboard.Name=="NDT")
                    {
                        var InspectionLists = Inspectionboard.Lists.ToList();
                        foreach (var InspectionList in InspectionLists)
                        {
                            int cards = 0;
                            var InspectionCards = InspectionList.Cards.ToList();
                            foreach (var InspectionCard in InspectionCards)
                            {
                                string LabelNote = "";
                                int ln = 1;
                                var labels = InspectionCard.Labels.ToList();
                                foreach (var label in labels)
                                {
                                    if (label.Name.ToLower().Contains("waiting"))
                                    {
                                        LabelNote = LabelNote + " Note" + ln + ": " + label.Name + ";";
                                        ln = ln + 1;
                                    }
                                    else
                                    {
                                    }
                                }
                                string CardName = InspectionCard.Name;
                                //Boolean Currentop = false;
                                //Boolean JobExists = false;
                                //String opStatus = "O";
                                //String opStartDate = "";
                                string MemberName = "";
                                string JobNum = InspectionCard.Name;
                                string JobNumWithoutTag = InspectionCard.Name;
                                var CardJobNumberList = new List<Class_CardJobNumbers>();

                                var cardMember = InspectionCard.Members.FirstOrDefault();
                                if (cardMember != null)
                                {
                                    MemberName = "(" + cardMember.FullName + ")";
                                }

                                //Thread.Sleep(500);
                                                               

                                var CardCheckLists = InspectionCard.CheckLists.ToList();
                                if (CardCheckLists.Count == 0)
                                {
                                    //get card name less anything after #
                                    int indexTag = InspectionCard.Name.IndexOf('#');
                                    if (indexTag == -1)
                                    {
                                        JobNumWithoutTag = InspectionCard.Name;
                                    }
                                    else
                                    {
                                        JobNumWithoutTag = InspectionCard.Name.Substring(0, indexTag);
                                    }

                                    //get card name prior to whitespace
                                    int index = InspectionCard.Name.IndexOf(' ') + 1;
                                    if (index == 0)
                                    {
                                        index = InspectionCard.Name.Length;
                                    }

                                    JobNum = InspectionCard.Name.Substring(0, index).Trim();

                                    var addtolist = new Class_CardJobNumbers();

                                    addtolist.cardName = JobNum;
                                    addtolist.cardJobNum = InspectionCard.Name;
                                    addtolist.cardIDQuid = InspectionCard.Id;
                                    addtolist.CardNameWithOutTags = JobNumWithoutTag;
                                    CardJobNumberList.Add(addtolist);

                                    string OSS = "";
                                    if (InspectionCard.Name.ToUpper().Contains("OSS"))
                                    {
                                        //JobNum = InspectionCard.Name.Replace("OSS", "").Trim();
                                        OSS = "OSS";
                                    }
                                    using (var contextPR = new ProductionReportEntities())
                                    {
                                        var job = contextPR.WorkCenterStats.Where(x => x.Job_Number.ToUpper().Trim() == JobNum.ToUpper() && x.Status == "O").FirstOrDefault();
                                        if (job == null)
                                        {
                                            Console.WriteLine("Not in JBView:  " + InspectionCard.Name);
                                            //CurrentOP(JobNum, ref Currentop, ref JobExists, ref opStatus, ref opStartDate);                                            
                                        }
                                        else
                                        {
                                            //add job to class
                                            if (!job.WC_Vendor.Contains("INSP") && !job.WC_Vendor.Contains("SOURCE"))
                                            {
                                                // what to do if current jobBOSS operation is not inspection OP?
                                                Console.WriteLine("Not Inspection OP:  " + job.Job_Number);
                                                //InspectionCard.Comments.Add("@michellehuffman3 Current Operation in JobBOSS is not inspection- please review");
                                            }
                                            else
                                            {
                                                string TrelloNote = job.WC_Vendor + ": " + OSS + " " + InspectionList.Name + " " + MemberName + LabelNote;
                                                var PRnotes = contextPR.Notes.Where(x => x.Job_Operation_Key == job.Job_Operation_Key && x.Notes == TrelloNote).FirstOrDefault();
                                                //if (job.LatestNote == TrelloNote)
                                                if(PRnotes!=null)
                                                {
                                                    Console.WriteLine("TrelloNote=LatestNote :  " + job.Job_Number);
                                                    //if (opStartDate != "" && InspectionCard.DueDate != DateTime.Parse(opStartDate) && Currentop == true)
                                                    //{
                                                    //    InspectionCard.DueDate = DateTime.Parse(opStartDate);                                                    
                                                    //    contextPR.SaveChanges();
                                                    //}
                                                }
                                                else
                                                {
                                                    //var addtolist = new Class_CardJobNumbers();
                                                    //addtolist.cardJobNum = JobNum;
                                                    //addtolist.JobOperationKey = job.Job_Operation_Key;
                                                    //CardJobNumberList.Add(addtolist);

                                                    //if (opStartDate == "")
                                                    //{
                                                    job.LatestNote = TrelloNote;
                                                    job.LatestNoteDate = timestamp;
                                                    //contextPR.SaveChanges();
                                                    //}
                                                    //else
                                                    //{
                                                    //    InspectionCard.DueDate = DateTime.Parse(opStartDate);
                                                    //    job.LatestNote = TrelloNote;
                                                    //    job.LatestNoteDate = timestamp;
                                                    //    contextPR.SaveChanges();
                                                    //}

                                                    contextPR.Notes.Add(new Note()
                                                    {
                                                        Job_Operation_Key = job.Job_Operation_Key,
                                                        User = "Acutec\\mhuffman",
                                                        Notes = TrelloNote,
                                                        NoteDate = timestamp,
                                                        NoteDate_Created = timestamp,
                                                        NoteDate_Modified = timestamp,
                                                        JobNumber = job.Job_Number,
                                                    });
                                                    Console.WriteLine("Update : " + job.Job_Number);
                                                    contextPR.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var cardchecklist in CardCheckLists)
                                    {
                                        var checkListItems = cardchecklist.CheckItems.ToList();
                                        foreach (var checklistItem in checkListItems)
                                        {
                                            string ChecklistItemStatus = checklistItem.State.Value.ToString();
                                            if (ChecklistItemStatus == "Complete")
                                            {
                                                Console.WriteLine(checklistItem.Name + "  Complete- Do nothing");
                                            }
                                            else
                                            {
                                                //get card name less anything after #
                                                int indexTag = InspectionCard.Name.IndexOf('#');
                                                if (indexTag == -1)
                                                {
                                                    JobNumWithoutTag = InspectionCard.Name;
                                                }
                                                else
                                                {
                                                    JobNumWithoutTag = InspectionCard.Name.Substring(0, indexTag);
                                                }
                                                int index = InspectionCard.Name.IndexOf(' ') + 1;
                                                if (index == 0)
                                                {
                                                    index = InspectionCard.Name.Length;
                                                }

                                                JobNum = InspectionCard.Name.Substring(0, index).Trim() + checklistItem.Name;
                                                //JobNum = InspectionCard.Name + checklistItem.Name;

                                                var addtolist = new Class_CardJobNumbers();
                                                addtolist.cardName = JobNum;
                                                addtolist.cardJobNum = InspectionCard.Name;
                                                addtolist.cardIDQuid = InspectionCard.Id;
                                                addtolist.CardNameWithOutTags = JobNumWithoutTag;
                                                CardJobNumberList.Add(addtolist);

                                                string OSS = "";

                                                if (cardchecklist.Name.ToUpper().Contains("OSS"))
                                                {
                                                    JobNum = InspectionCard.Name + cardchecklist.Name.Replace("OSS", "").Trim();
                                                    OSS = "OSS";
                                                }
                                                using (var contextPR = new ProductionReportEntities())
                                                {
                                                    var job = contextPR.WorkCenterStats.Where(x => x.Job_Number.ToUpper().Trim() == JobNum.ToUpper().Trim() && x.Status == "O").FirstOrDefault();

                                                    if (job == null)
                                                    {
                                                        Console.WriteLine("Not in JBView:  " + InspectionCard.Name);
                                                        //CurrentOP(JobNum, ref Currentop, ref JobExists, ref opStatus, ref opStartDate);  
                                                    }
                                                    else
                                                    {
                                                        if (!job.WC_Vendor.Contains("INSP") && !job.WC_Vendor.Contains("SOURCE"))
                                                        {
                                                            // what to do if current jobBOSS operation is not inspection OP?
                                                            Console.WriteLine("Not Inspection OP:  " + job.Job_Number);
                                                            //InspectionCard.Comments.Add("@michellehuffman3 Current Operation in JobBOSS is not inspection- please review");
                                                        }
                                                        else
                                                        {
                                                            //var addtolist = new Class_CardJobNumbers();
                                                            //addtolist.cardJobNum = JobNum;
                                                            //addtolist.JobOperationKey = job.Job_Operation_Key;                                                            
                                                            //CardJobNumberList.Add(addtolist);

                                                            string TrelloNote = job.WC_Vendor + ": " + OSS + " " + InspectionList.Name + " " + MemberName + LabelNote;
                                                            if (job.LatestNote == TrelloNote)
                                                            {
                                                                Console.WriteLine("TrelloNote=LatestNote :  " + job.Job_Number);
                                                                //if (opStartDate != "" && InspectionCard.DueDate != DateTime.Parse(opStartDate) && Currentop == true)
                                                                //{
                                                                //    InspectionCard.DueDate = DateTime.Parse(opStartDate);
                                                                //    contextPR.SaveChanges();
                                                                //}
                                                            }
                                                            else
                                                            {
                                                                //if (opStartDate == "")
                                                                //{
                                                                job.LatestNote = TrelloNote;
                                                                job.LatestNoteDate = timestamp;
                                                                //contextPR.SaveChanges();
                                                                //}
                                                                //else
                                                                //{
                                                                //    InspectionCard.DueDate = DateTime.Parse(opStartDate);
                                                                //    job.LatestNote = TrelloNote;
                                                                //    job.LatestNoteDate = timestamp;
                                                                //    contextPR.SaveChanges();
                                                                //}
                                                                contextPR.Notes.Add(new Note()
                                                                {
                                                                    Job_Operation_Key = job.Job_Operation_Key,
                                                                    User = "Acutec\\mhuffman",
                                                                    Notes = TrelloNote,
                                                                    NoteDate = timestamp,
                                                                    NoteDate_Created = timestamp,
                                                                    NoteDate_Modified = timestamp,
                                                                    JobNumber = job.Job_Number,
                                                                });
                                                                Console.WriteLine("Update : " + job.Job_Number);
                                                                contextPR.SaveChanges();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //}
                                }
                                //update card with Due Date if Schedule Start Date Available for inspection operation
                                Boolean Currentop = false;
                                Boolean JobExists = false;
                                String opStatus = "O";
                                String opStartDate = "";

                                foreach (var item in CardJobNumberList)
                                {
                                    item.AddNoteToCard = false;

                                    using (var contextJB = new AcutecSQLEntities())
                                    {
                                        var jobOperations = contextJB.Job_Operation.Where(x => x.Job == item.cardName && (x.Status == "O" || x.Status == "S")).ToList();
                                        if (jobOperations.Count == 0)
                                        {
                                            //unable to find trello job in JobBOSS- write code to add comment to card
                                            item.AddNoteToCard = true;
                                            item.NoteForCard = "Unable to find this Job Number in JobBOSS";

                                            var ExpediteCheckJobBosslabels = Inspectionboard.Labels.Where(x => x.Name.ToLower().Contains("expediter")).FirstOrDefault();
                                            if (ExpediteCheckJobBosslabels != null)
                                            {
                                                var checkCardForExpediteLabel = InspectionCard.Labels.Where(x => x.Name.ToLower().Contains("expediter")).FirstOrDefault();
                                                if (checkCardForExpediteLabel == null)
                                                {
                                                    if (InspectionList.Name.ToLower().Contains("complete"))
                                                    {
                                                    }
                                                    else
                                                    {                                                       
                                                        InspectionCard.Labels.Add(ExpediteCheckJobBosslabels);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var jobOperation = jobOperations.OrderBy(x => x.Sequence).First();
                                            if (!jobOperation.WC_Vendor.Contains("INSP") && !jobOperation.WC_Vendor.Contains("SOURCE"))
                                            {
                                                item.AddNoteToCard = true;
                                                //item.NoteForCard = "@michellehuffman3 Inspection/Source is not the current Operation In JobBOSS- please review";
                                                //InspectionCard.Comments.Add(item.NoteForCard);
                                                var ExpediteCheckJobBosslabels = Inspectionboard.Labels.Where(x => x.Name.ToLower().Contains("expediter")).FirstOrDefault();
                                                if (ExpediteCheckJobBosslabels != null)
                                                {
                                                    var checkCardForExpediteLabel = InspectionCard.Labels.Where(x => x.Name.ToLower().Contains("expediter")).FirstOrDefault();
                                                    if (checkCardForExpediteLabel == null)
                                                    {
                                                        if (InspectionList.Name.ToLower().Contains("complete"))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            InspectionCard.Labels.Add(ExpediteCheckJobBosslabels);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                string HashTag_Queue = "";
                                                if (InspectionList.Name.ToLower().Contains("complete"))
                                                {

                                                }
                                                else
                                                {
                                                    using (var contextQT = new QueueTrackerEntities())
                                                    {
                                                        var queJob = contextQT.WorkCenters.Where(x => x.JobOperation == jobOperation.Job_Operation1).FirstOrDefault();
                                                        if (queJob != null)
                                                        {
                                                            int OpDaycount = queJob.DayCount;
                                                            if (OpDaycount < 1)
                                                            {
                                                                OpDaycount = 1;
                                                            }
                                                            HashTag_Queue = " #" + OpDaycount.ToString();
                                                        }
                                                        else
                                                        {
                                                            using (var contextPR = new ProductionReportEntities())
                                                            {
                                                                var JobPR = contextPR.WorkCenterStats.Where(x => x.Job_Operation_Key == jobOperation.Job_Operation1).FirstOrDefault();
                                                                if (JobPR == null)
                                                                {
                                                                    HashTag_Queue = " #1";
                                                                }
                                                                else
                                                                {
                                                                    HashTag_Queue = " #" + JobPR.On_Report_Days.ToString();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (jobOperation.Sched_Start == null)
                                                {

                                                }
                                                else
                                                {
                                                    int CountDuplicateCardsInClass = CardJobNumberList.Where(x => x.cardIDQuid == item.cardIDQuid).Count();
                                                    if (CountDuplicateCardsInClass > 1)
                                                    {

                                                    }
                                                    else
                                                    {
                                                        if (InspectionCard.DueDate == null)
                                                        {
                                                            InspectionCard.DueDate = jobOperation.Sched_Start.Value.Date;

                                                            if (!InspectionCard.Name.Contains(HashTag_Queue))
                                                            {
                                                                InspectionCard.Name = item.CardNameWithOutTags + HashTag_Queue;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (InspectionCard.DueDate.Value.Date != jobOperation.Sched_Start.Value.Date)
                                                            {
                                                                InspectionCard.DueDate = jobOperation.Sched_Start;
                                                            }
                                                            if (!InspectionCard.Name.Contains(HashTag_Queue))
                                                            {
                                                                InspectionCard.Name = item.CardNameWithOutTags + HashTag_Queue;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //JobNum = item.cardName;
                                        //CurrentOP(JobNum, ref Currentop, ref JobExists, ref opStatus, ref opStartDate);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                }
            //}
            //catch (Exception e)
            //{
            //    //define email settings
            //    MailMessage message = new MailMessage();
            //    SmtpClient smtpClient = new SmtpClient();

            //    message.From = new MailAddress("acutecprecision@gmail.com", "Acutec Trello Notification");
            //    message.To.Add(new MailAddress("bmccrillis@acutecprecision.com", "Bruce McCrillis"));
            //    message.Subject = "Error Running Trello Inspection Update- Please fix";
            //    message.Body = "This is an automated email, do not reply" + Environment.NewLine + Environment.NewLine;
            //    message.Body = message.Body + e.Message + Environment.NewLine + Environment.NewLine;
            //    message.Body = message.Body + e.InnerException + Environment.NewLine + Environment.NewLine;

            //    smtpClient.Host = "smtp.gmail.com";
            //    smtpClient.Port = 587;
            //    smtpClient.UseDefaultCredentials = false;
            //    smtpClient.Timeout = 10000;
            //    smtpClient.Credentials = new System.Net.NetworkCredential("acutecprecision@gmail.com", "laughlin16891");
            //    smtpClient.EnableSsl = true;
            //    smtpClient.Send(message);
            //}
                //stopwatch.Stop();
                //Console.WriteLine("********************  Timer lapsed time (seconds) " + stopwatch.ElapsedMilliseconds/1000);
        }        
           
        
        private static void CurrentOP(String JobNum, ref Boolean Currentop, ref Boolean JobExists, ref String opStatus, ref String opStartDate)
        {
            Currentop = false;
            JobExists = false;
            opStartDate = "";
            
            using (var contextJB = new AcutecSQLEntities())
            {
                var JBjob = contextJB.Jobs.Where(x => x.Job1 == JobNum).FirstOrDefault();
                if(JBjob!=null)
                {
                    JobExists = true;
                    var JBop = contextJB.Job_Operation.Where(x => x.Job == JBjob.Job1 && x.Status != "C").OrderBy(y => y.Sequence).FirstOrDefault();
                    if (JBop == null)
                    {

                    }
                    else
                    {
                        if(JBop.Sched_Start!= null)
                        {
                            opStartDate = JBop.Sched_Start.Value.ToString("MM-dd-yyyy");
                        }
                        if (JBop.WC_Vendor.Contains("INSP") || JBop.WC_Vendor.Contains("SOURCE"))
                        {
                            Currentop = true;
                        }
                    }
                }
                //if (jobOp.Status == "C")
                //{
                //    CalculateSetup = true;
                //}
                //if (TotalActRunQty > 0)
                //{
                //    CalculateSetup = true;
                //}
            }
        } 
    }
}
