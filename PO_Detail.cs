//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class PO_Detail
    {
        public int PO_DetailKey { get; set; }
        public Nullable<int> PO_Detail1 { get; set; }
        public string PO { get; set; }
        public string Line { get; set; }
        public string Status { get; set; }
        public double Order_Quantity { get; set; }
        public string Purchase_Unit { get; set; }
        public Nullable<double> Unit_Cost { get; set; }
        public bool Certs_Required { get; set; }
        public Nullable<System.DateTime> Due_Date { get; set; }
        public string Vendor_Reference { get; set; }
        public string Cost_Unit { get; set; }
        public double Purchase_Unit_Weight { get; set; }
        public string Addl_Cost_Desc1 { get; set; }
        public string Addl_Cost_Desc2 { get; set; }
        public Nullable<decimal> Addl_Cost_Est_Amt1 { get; set; }
        public Nullable<decimal> Addl_Cost_Est_Amt2 { get; set; }
        public Nullable<decimal> Addl_Cost_Act_Amt1 { get; set; }
        public Nullable<decimal> Addl_Cost_Act_Amt2 { get; set; }
        public Nullable<decimal> Act_Cost { get; set; }
        public decimal Est_Ext_Amount { get; set; }
        public string Note_Text { get; set; }
        public string Ext_Description { get; set; }
        public System.DateTime Last_Updated { get; set; }
        public Nullable<int> PO_Line5x { get; set; }
        public string GL_Account { get; set; }
        public string Line_Revision { get; set; }
        public string Holder_ID { get; set; }
        public string Tax_Code { get; set; }
        public Nullable<bool> Addl_Cost_Taxable1 { get; set; }
        public Nullable<bool> Addl_Cost_Taxable2 { get; set; }
        public string Awarded_Vendor { get; set; }
        public Nullable<int> Lowest_Quote { get; set; }
        public Nullable<bool> Plan_Modified { get; set; }
        public Nullable<bool> Price_Locked { get; set; }
        public Nullable<int> PO_Type { get; set; }
        public System.Guid ObjectID { get; set; }
        public Nullable<System.Guid> PO_Header_OID { get; set; }
    }
}
