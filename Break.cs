//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Break
    {
        public System.Guid Breaks { get; set; }
        public string Break_Name { get; set; }
        public Nullable<System.DateTime> End_Time { get; set; }
        public Nullable<System.DateTime> Start_Time { get; set; }
        public Nullable<System.Guid> Shift { get; set; }
        public Nullable<int> PostEnd_Grace { get; set; }
        public Nullable<int> PostStart_Grace { get; set; }
        public Nullable<int> PreEnd_Grace { get; set; }
        public Nullable<int> PreStart_Grace { get; set; }
        public bool Deduct_Job_Time { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
        public Nullable<bool> Schedule_Break { get; set; }
        public Nullable<System.DateTime> Created_Time { get; set; }
    }
}
