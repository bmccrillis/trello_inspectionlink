//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trello_InspectionLink
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction_Data
    {
        public string Transaction_Data1 { get; set; }
        public Nullable<int> Transaction_Type { get; set; }
        public string Employee { get; set; }
        public Nullable<System.DateTime> Work_Date { get; set; }
        public string Terminal_ID { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> Transaction_Start { get; set; }
        public Nullable<System.DateTime> Transaction_End { get; set; }
        public string Error_ID { get; set; }
        public string Error_Text { get; set; }
        public Nullable<int> Linked_Tran_Int { get; set; }
        public string Linked_Tran_String { get; set; }
        public Nullable<int> Target_Int { get; set; }
        public string Target_String { get; set; }
        public string Job { get; set; }
        public string Material { get; set; }
        public Nullable<double> Quantity { get; set; }
        public string Document { get; set; }
        public string Location { get; set; }
        public string Location_1 { get; set; }
        public string Lot { get; set; }
        public string Lot_1 { get; set; }
        public string PO { get; set; }
        public string PO_Line { get; set; }
        public Nullable<System.DateTime> Last_Updated { get; set; }
        public Nullable<bool> Close { get; set; }
    }
}
